function setCurrentTime() {
    var myDate = new Date();

    let daysList = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];
    let monthsList = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Aug",
        "Oct",
        "Nov",
        "Dec",
    ];

    let date = myDate.getDate();
    let month = monthsList[myDate.getMonth()];
    let year = myDate.getFullYear();
    let day = daysList[myDate.getDay()];

    let today = `${date} ${month} ${year}, ${day}`;

    let amOrPm;
    let twelveHours = function() {
        if (myDate.getHours() > 12) {
            amOrPm = "PM";
            let twentyFourHourTime = myDate.getHours();
            let conversion = twentyFourHourTime - 12;
            return `${conversion}`;
        } else {
            amOrPm = "AM";
            return `${myDate.getHours()}`;
        }
    };
    let hours = twelveHours();
    let minutes = myDate.getMinutes();
    let seconds = myDate.getSeconds();

    let currentTime = `${hours}:${minutes}:${seconds} ${amOrPm}`;

    document.getElementById("date-time").innerText = today + " " + currentTime;
}

setInterval(function() {
    setCurrentTime();
}, 1000);

let startTime = document.getElementById("start");
let button = document.getElementById("click");
let stop = document.getElementById("stop");
let isStarted = true;
let isStopped = false;
let startTimeAsMinutes;
let stopTimeAsMinutes;
let totalMinutes = document.getElementById("total_minutes");
let money = document.getElementById("paid");
let clearbtn = false;

function timer() {
    if (isStarted && !isStopped) {
        let date = new Date();
        startTimeAsMinutes = date.getHours() * 60 + date.getMinutes();
        startTime.innerHTML = date.toLocaleTimeString("en-us");
        button.style.backgroundColor = "red";
        button.innerHTML = "stop";
        isStarted = false;
        isStopped = true;
        console.log("block1");
    } else if (!isStarted && isStopped) {
        console.log("block2");

        button.style.backgroundColor = "yellow";
        button.innerHTML = "clear";
        let date = new Date();
        stopTimeAsMinutes = date.getHours() * 60 + date.getMinutes();
        let stopTime = stopTimeAsMinutes - startTimeAsMinutes;
        stop.innerHTML = date.toLocaleTimeString("en-us");
        totalMinutes.innerHTML = stopTime;

        console.log(stopTime);

        if ((stopTime) => 0 && stopTime < 15) {
            console.log("");
            money.innerHTML = 500;
        } else if (stopTime > 16 && stopTime < 30) {
            money.innerHTML = 1000;
        } else if (stopTime > 31 && stopTime < 60) {
            money.innerHTML = 1500;
        } else {
            if (stopTime % 60 == 0) {
                money.innerHTML = (stopTime / 60) * 1500;
            } else if (stopTime % 60 <= 15) {
                money.innerHTML = Math.trunc(stopTime / 60) * 1500 + 1000;
            } else if (stopTime % 60 <= 30) {
                money.innerHTML = Math.trunc(stopTime / 60) * 1500 + 1000;
            } else if (stopTime % 60 <= 60) {
                money.innerHTML = Math.trunc(stopTime / 60) * 1500 + 1500;
            }
        }
        isStopped = false;
        isStarted = false;

    } else {
        let date = new Date();
        console.log("object");
        isStarted = true;
        isStopped = false;
        button.style.backgroundColor = "green";

        startTime.innerHTML = "0:00";
        stop.innerHTML = "0.00";
        totalMinutes.innerHTML = "0";
        money.innerHTML = "0";

    }
}