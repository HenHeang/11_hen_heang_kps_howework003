tailwind.config = {
  theme: {
    extend: {
      colors: {
        clifford: "#da373d",
      },
      backgroundImage: {
        "bg-image": "url('./Image/architecture-1048092_960_720.jpg')",
      },
    },
  },
};
